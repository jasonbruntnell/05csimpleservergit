/*first line*/
import * as express from 'express'
import {Controller} from "./Controller"
//second line
export class App {
    private static port:number = 3000;
    private app: any; //type will be function
    //private router:any;
    private controller: Controller;

    private constructor(){
        this.app = express(); // generate express-application
        //this.router = express.Router();
        this.controller = new Controller;
    }

    public static newInstance(){
        return new App();
    }

    public startRouter():void{
        //Home page route
        this.app.get("/",this.controller.showIndexPage);

        //About page route
        this.app.get(/marathon..$/,this.controller.showMarathonPage);   //...ur contains marathon
        this.app.get(/book..$/,this.controller.showBookPage);           //...ur contains book/#
        this.app.get(/(images|stylesheets)/,this.controller.showImagesAndCSS);  //...url
        this.app.all("*",this.controller.showErrorPage);
        this.app.use(this.controller.doErrorHandling);      
        this.app.listen(App.port, function (){
            console.log("web server app listening on port " + App.port);
        });
    }
}

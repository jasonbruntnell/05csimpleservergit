import {FileOptions} from "./FileOptions";
import {AppError} from "./AppError";
/** 
 * here could be eg. Car/Book-objects
 * as we deal with static pages, there is no neded for that
*/

export class Controller {

    private static options: FileOptions; //instance-var undefined in CBf show...Page()
    public constructor(){
        Controller.options = new FileOptions();
        Controller.options.root = "/Users/schueler/sourceVSCode/05SimpleServer/05CSimpleServer/wwwPublic";
    }
    public showIndexPage(req,res):void{
        res.sendFile("html/index.html",Controller.options);
    }
    public showMarathonPage(req,res):void{
        let url:string = req.originalUrl;
        let numberMarathon:string=url.charAt(url.length-1);
        console.log("=====show marathon..."+url);
        res.sendFile("html/marathon"+ numberMarathon +".html",Controller.options);
    }
    public showImagesAndCSS(req,res):void{
        let url:string = req.originalUrl;
        console.log("======show images/CSS..."+url);
        res.sendFile(url,Controller.options);
    }
    public showBookPage(req,res,next):void{
        let url:string = req.originalUrl;
        let numberBook:string=url.charAt(url.length-1);
        console.log("=====show Book..."+url);
        if (numberBook > "4"){
            let err: AppError = new AppError("Can't find book-id" + numberBook + " on Server!");
            err.status = 'fail';
            err.statusCode = 404;
            next(err);
        }
        res.sendFile("html/book"+ numberBook +".html",Controller.options);
    }
    public showErrorPage(req,res):void{
        console.log("======error page ..." + req.originalUrl);
        res.status(404).json({
            status: "fail",
            message: "Can't find " + req.originalUrl + " on this server!"
        });
        //res.send("<p>message: Cant find site on this server</p>");
    }
    public doErrorHandling(err: AppError, req, res, next) :void {
        console.log("====error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);
    }
}
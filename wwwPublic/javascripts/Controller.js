"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const FileOptions_1 = require("./FileOptions");
const AppError_1 = require("./AppError");
/**
 * here could be eg. Car/Book-objects
 * as we deal with static pages, there is no neded for that
*/
class Controller {
    constructor() {
        Controller.options = new FileOptions_1.FileOptions();
        Controller.options.root = "/Users/schueler/sourceVSCode/05SimpleServer/05CSimpleServer/wwwPublic";
    }
    showIndexPage(req, res) {
        res.sendFile("html/index.html", Controller.options);
    }
    showMarathonPage(req, res) {
        let url = req.originalUrl;
        let numberMarathon = url.charAt(url.length - 1);
        console.log("=====show marathon..." + url);
        res.sendFile("html/marathon" + numberMarathon + ".html", Controller.options);
    }
    showImagesAndCSS(req, res) {
        let url = req.originalUrl;
        console.log("======show images/CSS..." + url);
        res.sendFile(url, Controller.options);
    }
    showBookPage(req, res, next) {
        let url = req.originalUrl;
        let numberBook = url.charAt(url.length - 1);
        console.log("=====show Book..." + url);
        if (numberBook > "4") {
            let err = new AppError_1.AppError("Can't find book-id" + numberBook + "on Server!");
            err.status = 'fail';
            err.statusCode = 404;
            next(err);
        }
        res.sendFile("html/book" + numberBook + ".html", Controller.options);
    }
    showErrorPage(req, res) {
        console.log("======error page ..." + req.originalUrl);
        res.status(404).json({
            status: "fail",
            message: "Can't find " + req.originalUrl + " on this server!"
        });
        //res.send("<p>message: Cant find site on this server</p>");
    }
    doErrorHandling(err, req, res, next) {
        console.log("====error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);
    }
}
exports.Controller = Controller;
//# sourceMappingURL=Controller.js.map
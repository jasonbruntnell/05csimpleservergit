"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const express = require("express");
const Controller_1 = require("./Controller");
class App {
    constructor() {
        this.app = express(); // generate express-application
        //this.router = express.Router();
        this.controller = new Controller_1.Controller;
    }
    static newInstance() {
        return new App();
    }
    startRouter() {
        //Home page route
        this.app.get("/", this.controller.showIndexPage);
        //About page route
        this.app.get(/marathon..$/, this.controller.showMarathonPage); //...ur contains marathon
        this.app.get(/book..$/, this.controller.showBookPage); //...ur contains book/#
        this.app.get(/(images|stylesheets)/, this.controller.showImagesAndCSS); //...url
        this.app.all("*", this.controller.showErrorPage);
        this.app.use(this.controller.doErrorHandling);
        this.app.listen(App.port, function () {
            console.log("web server app listening on port " + App.port);
        });
    }
}
exports.App = App;
App.port = 3000;
//# sourceMappingURL=App.js.map